﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//Import SFML
using SFML.Graphics;
//Import farseer physics
using FarseerPhysics.Collision;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using FarseerPhysics;
namespace LD30
{
    class Block
    {
        //Physics
        public Body body;
        //Graphics
        public int x, y, id = 0;
        public Block(int x, int y, int id)
        {
            this.id = id;
            this.x = x;
            this.y = y;
            this.body = BodyFactory.CreateRectangle(Game.world, 35 * .1f, 35 * .1f, 4.0f);
            this.body.FixedRotation = true;
            this.body.Awake = false;
            this.body.BodyType = BodyType.Static;
            body.Position = new Vector2(x * .1f, y * .1f);
            body.CollisionCategories = Category.Cat5;
            body.SleepingAllowed = true;
        }
        public static string getTexture(int id)
        {
            switch (id)
            {
                case 1: return "content/images/Blocks/dirtMid.png";
                case 2: return "content/images/Blocks/dirtCenter.png";
                default: return "content/images/Blocks/error.png";
            }
        }
    }
}
