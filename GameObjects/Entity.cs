﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//Import SFML
using SFML.Graphics;
using SFML.Window;
//Import
using FarseerPhysics.Collision;
using FarseerPhysics.Factories;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Common;
using FarseerPhysics.Controllers;
using FarseerPhysics;
namespace LD30
{
    class Entity
    {
        public int x, y;
        public Sprite sprite;
        public Texture texture;
        //Physics
        public Body body;
        //Animation
        public int Frame = 1;
        public string image; 
        public Entity(int x, int y, string image)
        {
            this.image = image;
            this.x = x;
            this.y = y;
            this.texture = new Texture(image + Frame.ToString() + ".png");
            this.sprite = new Sprite(texture);
            body = BodyFactory.CreateRectangle(Game.world, (sprite.Texture.Size.X), (sprite.Texture.Size.Y), 19f);
            this.body.BodyType = BodyType.Dynamic;
            body.Position = new Microsoft.Xna.Framework.Vector2((x), (y));
        }
        public void Move(Vector2f vector)
        {
            sprite.Position = vector;
        }

        public void Animate(int i){
            if(Frame <= 10){
                Frame++;
            }
            else
            {
                Frame = 1;
            }
            if (i == 0)
            {
                sprite.Texture = new Texture(image + Frame.ToString() + ".png");
                sprite.Scale = new Vector2f(1, 1);
            }
            if(i == 1){
                sprite.Texture = new Texture(image + Frame.ToString() + ".png");
                sprite.Scale = new Vector2f(-1,1);
            }
        }
    }
}
